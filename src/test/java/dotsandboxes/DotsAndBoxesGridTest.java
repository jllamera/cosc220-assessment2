package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static
     * fields.
     * This field is a logger. Loggers are like a more advanced println, for writing
     * messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);
    private DotsAndBoxesGrid gridTest;

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does
     * something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @BeforeEach
    public void setup() {
        // Setting up a new DotsAndBoxesGrid for every test
        gridTest = new DotsAndBoxesGrid(4, 4, 2);
    }

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testBoxCompletion() {
        logger.info("Testing box completion");

        gridTest.drawHorizontal(0, 0, 1);
        gridTest.drawVertical(0, 0, 1);
        gridTest.drawHorizontal(0, 1, 1);
        gridTest.drawVertical(1, 0, 1);

        assertTrue(gridTest.boxComplete(0,0), "Box should be completed");

    }

    @Test
    public void testVerticalLineRedrawn() {
        logger.info("Testing if throws exception if redrawn a line  ");
        gridTest.drawVertical(0, 0, 1);

        assertThrows(IllegalStateException.class, () -> gridTest.drawVertical(0, 0, 1));

    }
}
